# Translation of doc.sk.po
# Copyright (C)
# This file is distributed under the same license as the webwml package.
# Ivan Masár <helix84@centrum.sk>, 2009, 2014, 2017.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2017-07-12 16:39+0200\n"
"Last-Translator: Ivan Masár <helix84@centrum.sk>\n"
"Language-Team: x\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Virtaal 0.7.1\n"

#: ../../english/doc/books.data:31
msgid ""
"\n"
"  Debian 8 is the must-have handbook for learning Linux. Start on the\n"
"  beginners level and learn how to deploy the system with graphical\n"
"  interface and terminal.\n"
"  This book provides the basic knowledge to grow and become a 'junior'\n"
"  systems administrator. Start off with exploring the GNOME desktop\n"
"  interface and adjust it to your personal needs. Overcome your fear of\n"
"  using the Linux terminal and learn the most essential commands in\n"
"  administering Debian. Expand your knowledge of system services (systemd)\n"
"  and learn how to adapt them. Get more out of the software in Debian and\n"
"  outside of Debian. Manage your home-network with network-manager, etc.\n"
"  10 percent of the profits on this book will be donated to the Debian\n"
"  Project."
msgstr ""
"\n"
"  Debian 8 je príručka na výuku Linuxu,ktorú jednoducho musíte mať. Začnite "
"na úrovni\n"
"  začiatočníka a naučte sa nasadiť systém pomocou grafického\n"
"  rozhrania a terminálu.\n"
"  Táto kniha poskytuje základné poznatky, ktoré potrebujete, aby ste mohli "
"rásť a stať sa tzv. „junior“\n"
"  správcom systému. Začnite tým, že preskúmate rozhranie pracovného "
"prostredia GNOME\n"
"  a prispôsobíte ho svojim osobným potrebám. Prekonajte svoj strach\n"
"  z používania linuxového terminálu a naučte sa najdôležitejšie príkazy\n"
"  na správu Debianu. Rozšírte svoje vedomosti o systémových službách "
"(systemd)\n"
"  a naučte sa ich prispôsobiť. Vyťažte viac zo softvéru v Debiane\n"
"  a mimo Debianu. Spravujte svoju domácu sieť pomocou network-manager atď.\n"
"  10 percent ziskov z tejto knihy bude venovaných projektu Debian."

#: ../../english/doc/books.data:59 ../../english/doc/books.data:185
#: ../../english/doc/books.data:211
#| msgid ""
#| "Written by two Debian developers, this free book\n"
#| "  started as a translation of their French best-seller known as Cahier "
#| "de\n"
#| "  l'admin Debian (published by Eyrolles). Accessible to all, this book\n"
#| "  teaches the essentials to anyone who wants to become an effective and\n"
#| "  independent Debian GNU/Linux administrator.  \n"
#| "  It covers all the topics that a competent Linux administrator should\n"
#| "  master, from the installation and the update of the system, up to the\n"
#| "  creation of packages and the compilation of the kernel, but also\n"
#| "  monitoring, backup and migration, without forgetting advanced topics\n"
#| "  like SELinux setup to secure services, automated installations, or\n"
#| "  virtualization with Xen, KVM or LXC."
msgid ""
"Written by two Debian developers, this free book\n"
"  started as a translation of their French best-seller known as Cahier de\n"
"  l'admin Debian (published by Eyrolles). Accessible to all, this book\n"
"  teaches the essentials to anyone who wants to become an effective and\n"
"  independent Debian GNU/Linux administrator.\n"
"  It covers all the topics that a competent Linux administrator should\n"
"  master, from the installation and the update of the system, up to the\n"
"  creation of packages and the compilation of the kernel, but also\n"
"  monitoring, backup and migration, without forgetting advanced topics\n"
"  like SELinux setup to secure services, automated installations, or\n"
"  virtualization with Xen, KVM or LXC."
msgstr ""
"Táto slobodná kniha, ktorú napísali dvaja vývojári Debianu,\n"
"  začala ako preklad ich francúzskeho bestselleru Cahier de\n"
"  l'admin Debian (vydalo nakladateľstvo Eyrolles). Táto kniha je prístupné "
"pre každého\n"
"  a naučí základy kohokoľvek, kto sa chce stať efektívnym a\n"
"  nezávislým správcom systému Debian GNU/Linux.\n"
"  Pokrýva všetky témy, ktoré by mal kompetentný správca Linuxu\n"
"  zvládnuť, od inštalácie a aktualizácie systému po\n"
"  tvorbu balíkov a kompiláciu jadra, ale aj\n"
"  monitorovanie, zálohovanie a migráciu. Neopomína ani pokročilá témy\n"
"  ako SELinux na zabezpečenie služieb, automatizované inštalácie alebo\n"
"  virtualizáciu pomocou Xen, KVM alebo LXC."

#: ../../english/doc/books.data:80
msgid ""
"The aim of this freely available, up-to-date, book is to get you up to\n"
"  speed with Debian (including both the current stable release and the\n"
"  current unstable distribution). It is comprehensive with basic support\n"
"  for the user who installs and maintains the system themselves (whether\n"
"  in the home, office, club, or school)."
msgstr ""
"Cieľom tejto voľne dostupnej a aktuálnej knihy je naučiť vás viac\n"
"  o Debiane (vrátane aktuálneho stabilného vydania a\n"
"  aktuálnej nestabilnej distribúcie). Poskytuje vyčerpávajúci výklad ohľadne "
"základnej podpory\n"
"  používateľa, ktorý si sám nainštaluje a spravuje systém (či už\n"
"  v domácnosti, v kancelárii, v klube alebo v škole)."

#: ../../english/doc/books.data:93
msgid "Debian 2.2 ARM architecture"
msgstr "Debian 2.2 architektúra ARM"

#: ../../english/doc/books.data:96
msgid ""
"This book is for developers working with GNU/Linux on ARM processors.\n"
"  It covers some devices specifically for quick set-up (LART, Assabet, "
"Psion5).\n"
"  It also gives generic information, tools and techniques. The GNU\n"
"  toolchain is covered in native and cross-compiler form, as are "
"bootloaders,\n"
"  kernel patches, RAMdisks, ARM peculiarities and other ARM resources. It "
"also\n"
"  includes extensive tutorial information on GNU/Linux basics for "
"developers\n"
"  coming from other platforms.\n"
"  It is not specific to Debian, but the Aleph\n"
"  ARMLinux distribution is closely based on Debian-ARM and it is assumed "
"you\n"
"  will be working from this distribution and preferably have a Debian based\n"
"  host machine too."
msgstr ""

#: ../../english/doc/books.data:123
msgid ""
"The first French book about Debian is already in its fifth edition. It\n"
"  covers all aspects of the administration of Debian from the installation\n"
"  to the configuration of network services.\n"
"  Written by two Debian developers, this book can be of interest to many\n"
"  people: the beginner wishing to discover Debian, the advanced user "
"looking\n"
"  for tips to enhance his mastership of the Debian tools and the\n"
"  administrator who wants to build a reliable network with Debian."
msgstr ""

#: ../../english/doc/books.data:142
msgid ""
"The book covers topics ranging from concepts of package\n"
"  management over the available tools and how they're used to concrete "
"problems\n"
"  which may occur in real life and how they can be solved. The book is "
"written\n"
"  in German, an English translation is planned. Format: e-book (Online, "
"HTML,\n"
"  PDF, ePub, Mobi), printed book planned.\n"
msgstr ""

#: ../../english/doc/books.data:162
msgid ""
"This book teaches you how to install and configure the system and also how "
"to use Debian in a professional environment.  It shows the full potential of "
"the distribution (in its current version 8) and provides a practical manual "
"for all users who want to learn more about Debian and its range of services."
msgstr ""

#: ../../english/doc/books.def:38
msgid "Author:"
msgstr "Autor:"

#: ../../english/doc/books.def:41
msgid "email:"
msgstr "email:"

#: ../../english/doc/books.def:45
msgid "Available at:"
msgstr "Dostupné na:"

#: ../../english/doc/books.def:48
msgid "CD Included:"
msgstr "Obsahuje CD:"

#: ../../english/doc/books.def:51
msgid "Publisher:"
msgstr "Vydavateľ:"

#: ../../english/doc/manuals.defs:28
msgid "Authors:"
msgstr "Autori:"

#: ../../english/doc/manuals.defs:35
msgid "Editors:"
msgstr "Redaktori:"

#: ../../english/doc/manuals.defs:42
msgid "Maintainer:"
msgstr "Správca:"

#: ../../english/doc/manuals.defs:49
msgid "Status:"
msgstr "Stav:"

#: ../../english/doc/manuals.defs:56
msgid "Availability:"
msgstr "Dostupnosť:"

#: ../../english/doc/manuals.defs:92
msgid "Latest version:"
msgstr "Najnovšia verzia:"

#: ../../english/doc/manuals.defs:108
msgid "(version <get-var version />)"
msgstr "(verzia <get-var version />)"

#: ../../english/doc/manuals.defs:138 ../../english/releases/arches.data:37
msgid "plain text"
msgstr "čistý text"

#: ../../english/doc/manuals.defs:154
msgid ""
"The latest <get-var srctype /> source is available through the <a href="
"\"https://www.debian.org/doc/cvs\">Subversion</a> repository."
msgstr ""
"Najnovší <get-var srctype /> zdrojový kód je dostupný z repozitára <a href=\""
"https://www.debian.org/doc/cvs\">Subversion</a>."

#: ../../english/doc/manuals.defs:156 ../../english/doc/manuals.defs:164
#: ../../english/doc/manuals.defs:172 ../../english/doc/manuals.defs:180
msgid "Web interface: "
msgstr "Webové rozhranie:"

#: ../../english/doc/manuals.defs:157 ../../english/doc/manuals.defs:165
#: ../../english/doc/manuals.defs:173 ../../english/doc/manuals.defs:181
msgid "VCS interface: "
msgstr "Rozhranie systému na správu verzií:"

#: ../../english/doc/manuals.defs:162
msgid ""
"The latest <get-var srctype /> source is available through the <a href="
"\"https://packages.debian.org/cvs\">Cvs</a> repository."
msgstr ""
"Najnovší <get-var srctype /> zdrojový kód je dostupný z repozitára <a href=\""
"https://packages.debian.org/cvs\">CVS</a>."

#: ../../english/doc/manuals.defs:170
msgid ""
"The latest <get-var srctype /> source is available through the <a href="
"\"https://packages.debian.org/subversion\">Subversion</a> repository."
msgstr ""
"Najnovší <get-var srctype /> zdrojový kód je dostupný z repozitára <a href=\""
"https://packages.debian.org/subversion\">Subversion</a>."

#: ../../english/doc/manuals.defs:178
msgid ""
"The latest <get-var srctype /> source is available through the <a href="
"\"https://packages.debian.org/git\">Git</a> repository."
msgstr ""
"Najnovší <get-var srctype /> zdrojový kód je dostupný z repozitára <a href=\""
"https://packages.debian.org/git\">Git</a>."

#: ../../english/doc/manuals.defs:188
msgid ""
"CVS sources working copy: set <code>CVSROOT</code>\n"
"  to <kbd>:ext:<var>userid</var>@cvs.debian.org:/cvs/debian-boot</kbd>,\n"
"  and check out the <kbd>boot-floppies/documentation</kbd> module."
msgstr ""
"Pracovná kópia zdrojových textov v CVS: nastavte <code>CVSROOT</code>\n"
"  na <kbd>:ext:<var>userid</var>@cvs.debian.org:/cvs/debian-boot</kbd>,\n"
"  a vykonajte checkout modulu <kbd>boot-floppies/documentation</kbd>."

#: ../../english/doc/manuals.defs:193
msgid "CVS via web"
msgstr "Webové rozhranie k CVS"

#: ../../english/doc/manuals.defs:197 ../../english/doc/manuals.defs:201
msgid "Debian package"
msgstr "Balík Debianu"

#: ../../english/doc/manuals.defs:206 ../../english/doc/manuals.defs:210
msgid "Debian package (archived)"
msgstr "Balík Debianu (archivovaný)"

#: ../../english/releases/arches.data:35
msgid "HTML"
msgstr "HTML"

#: ../../english/releases/arches.data:36
msgid "PDF"
msgstr "PDF"

#~ msgid ""
#~ "Use <a href=\"cvs\">SVN</a> to download the SGML source text for <get-var "
#~ "ddp_pkg_loc />."
#~ msgstr ""
#~ "Pomocou <a href=\"cvs\">SVN</a> si môžete stiahnuť zdrojový text <get-var "
#~ "ddp_pkg_loc /> v SGML."

#~ msgid "Language:"
#~ msgstr "Jazyk:"
