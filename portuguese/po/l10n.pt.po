# Brazilian Portuguese translation for Debian website l10n.pot
# Copyright (C) 2003-2009 Software in the Public Interest, Inc.
#
# Michelle Ribeiro <michelle@cipsga.org.br>, 2003
# Gustavo R. Montesino <grmontesino@ig.com.br>, 2004
# Felipe Augusto van de Wiel (faw) <faw@debian.org>, 2006-2009.
#
msgid ""
msgstr ""
"Project-Id-Version: Debian Webwml\n"
"PO-Revision-Date: 2009-01-05 23:17-0200\n"
"Last-Translator: Felipe Augusto van de Wiel (faw) <faw@debian.org>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/international/l10n/dtc.def:10
msgid "File"
msgstr "Arquivo"

#: ../../english/international/l10n/dtc.def:14
msgid "Package"
msgstr "Pacote"

#: ../../english/international/l10n/dtc.def:18
msgid "Score"
msgstr "Pontuação"

#: ../../english/international/l10n/dtc.def:22
msgid "Translator"
msgstr "Tradutor"

#: ../../english/international/l10n/dtc.def:26
msgid "Team"
msgstr "Equipe"

#: ../../english/international/l10n/dtc.def:30
msgid "Date"
msgstr "Data"

#: ../../english/international/l10n/dtc.def:34
msgid "Status"
msgstr "Estado"

#: ../../english/international/l10n/dtc.def:38
msgid "Strings"
msgstr "Strings"

#: ../../english/international/l10n/dtc.def:42
msgid "Bug"
msgstr "Bug"

#: ../../english/international/l10n/dtc.def:49
msgid "<get-var lang />, as spoken in <get-var country />"
msgstr "<get-var lang /> - <get-var country />"

#: ../../english/international/l10n/dtc.def:54
msgid "Unknown language"
msgstr "Idioma desconhecido"

#: ../../english/international/l10n/dtc.def:64
msgid "This page was generated with data collected on: <get-var date />."
msgstr "Esta página foi gerada com dados obtidos em: <get-var date />."

#: ../../english/international/l10n/dtc.def:69
msgid "Before working on these files, make sure they are up to date!"
msgstr ""
"Antes de trabalhar nestes arquivos, certifique-se de que eles estejam "
"atualizados!"

#: ../../english/international/l10n/dtc.def:79
msgid "Section: <get-var name />"
msgstr "Seção: <get-var name />"

#: ../../english/international/l10n/menu.def:10
msgid "L10n"
msgstr "L10n"

#: ../../english/international/l10n/menu.def:14
msgid "Language list"
msgstr "Lista de idiomas"

#: ../../english/international/l10n/menu.def:18
msgid "Ranking"
msgstr "Classificação"

#: ../../english/international/l10n/menu.def:22
msgid "Hints"
msgstr "Dicas"

#: ../../english/international/l10n/menu.def:26
msgid "Errors"
msgstr "Erros"

#: ../../english/international/l10n/menu.def:30
msgid "POT files"
msgstr "Arquivos POT"

#: ../../english/international/l10n/menu.def:34
msgid "Hints for translators"
msgstr "Dicas para tradutores"

#~ msgid "Translated templates"
#~ msgstr "Modelos traduzidos"

#~ msgid "Original templates"
#~ msgstr "Modelo original"
