msgid ""
msgstr ""
"Project-Id-Version: organization.po\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2016-12-22 02:31+0900\n"
"Last-Translator: Nobuhiro Iwamatsu <iwamatsu@debian.org>\n"
"Language-Team: Japanese <debian-www@debian.or.jp>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr "委任のメール"

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr "指名のメール"

#. One male delegate
#: ../../english/intro/organization.data:18
msgid "<void id=\"male\"/>delegate"
msgstr "委任"

#. One female delegate
#: ../../english/intro/organization.data:20
msgid "<void id=\"female\"/>delegate"
msgstr "委任"

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:25
msgid "current"
msgstr "現職"

#: ../../english/intro/organization.data:27
#: ../../english/intro/organization.data:29
msgid "member"
msgstr "構成員"

#: ../../english/intro/organization.data:32
msgid "manager"
msgstr "マネージャー"

#: ../../english/intro/organization.data:34
msgid "SRM"
msgstr "SRM"

#: ../../english/intro/organization.data:34
msgid "Stable Release Manager"
msgstr "安定版リリース管理者"

#: ../../english/intro/organization.data:36
msgid "wizard"
msgstr "ウィザード"

#: ../../english/intro/organization.data:39
msgid "chairman"
msgstr "委員長"

#: ../../english/intro/organization.data:42
msgid "assistant"
msgstr "補佐"

#: ../../english/intro/organization.data:44
msgid "secretary"
msgstr "書記"

#: ../../english/intro/organization.data:53
#: ../../english/intro/organization.data:63
msgid "Officers"
msgstr "執行部"

#: ../../english/intro/organization.data:54
#: ../../english/intro/organization.data:80
msgid "Distribution"
msgstr "ディストリビューション"

#: ../../english/intro/organization.data:55
#: ../../english/intro/organization.data:224
msgid "Communication and Outreach"
msgstr ""

#: ../../english/intro/organization.data:56
#: ../../english/intro/organization.data:227
msgid "Publicity team"
msgstr "広報チーム"

#: ../../english/intro/organization.data:57
#: ../../english/intro/organization.data:295
msgid "Support and Infrastructure"
msgstr "サポートと設備"

#. formerly Custom Debian Distributions (CCDs); see https://blends.debian.org/blends/ch-about.en.html#s-Blends
#: ../../english/intro/organization.data:59
msgid "Debian Pure Blends"
msgstr "Debian Pure Blends"

#: ../../english/intro/organization.data:66
msgid "Leader"
msgstr "リーダー"

#: ../../english/intro/organization.data:68
msgid "Technical Committee"
msgstr "技術委員会"

#: ../../english/intro/organization.data:75
msgid "Secretary"
msgstr "書記"

#: ../../english/intro/organization.data:83
msgid "Development Projects"
msgstr "開発プロジェクト"

#: ../../english/intro/organization.data:84
msgid "FTP Archives"
msgstr "FTP アーカイブ"

#: ../../english/intro/organization.data:86
msgid "FTP Masters"
msgstr "FTP マスター"

#: ../../english/intro/organization.data:90
msgid "FTP Assistants"
msgstr "FTP アシスタント"

#: ../../english/intro/organization.data:99
msgid "FTP Wizards"
msgstr "FTP ウィザード"

#: ../../english/intro/organization.data:103
msgid "Backports"
msgstr "Backports"

#: ../../english/intro/organization.data:105
msgid "Backports Team"
msgstr "Backports チーム"

#: ../../english/intro/organization.data:109
msgid "Individual Packages"
msgstr "個々のパッケージ"

#: ../../english/intro/organization.data:110
msgid "Release Management"
msgstr "リリース管理"

#: ../../english/intro/organization.data:112
msgid "Release Team"
msgstr "リリースチーム"

#: ../../english/intro/organization.data:125
msgid "Quality Assurance"
msgstr "品質保証"

#: ../../english/intro/organization.data:126
msgid "Installation System Team"
msgstr "インストールシステムチーム"

#: ../../english/intro/organization.data:127
msgid "Release Notes"
msgstr "リリースノート"

#: ../../english/intro/organization.data:129
msgid "CD Images"
msgstr "CD イメージ"

#: ../../english/intro/organization.data:131
msgid "Production"
msgstr "製品"

#: ../../english/intro/organization.data:139
msgid "Testing"
msgstr "テスト"

#: ../../english/intro/organization.data:141
msgid "Autobuilding infrastructure"
msgstr "オートビルドインフラ"

#: ../../english/intro/organization.data:143
msgid "Wanna-build team"
msgstr "ビルド要求チーム"

#: ../../english/intro/organization.data:151
msgid "Buildd administration"
msgstr "ビルドデーモン管理"

#: ../../english/intro/organization.data:170
msgid "Documentation"
msgstr "ドキュメンテーション"

#: ../../english/intro/organization.data:175
msgid "Work-Needing and Prospective Packages list"
msgstr "作業の必要なパッケージ一覧"

#: ../../english/intro/organization.data:178
msgid "Live System Team"
msgstr "ライブシステムチーム"

#: ../../english/intro/organization.data:179
msgid "Ports"
msgstr "移植"

#: ../../english/intro/organization.data:214
msgid "Special Configurations"
msgstr "特殊な設定"

#: ../../english/intro/organization.data:217
msgid "Laptops"
msgstr "ラップトップ"

#: ../../english/intro/organization.data:218
msgid "Firewalls"
msgstr "ファイアウォール"

#: ../../english/intro/organization.data:219
msgid "Embedded systems"
msgstr "組み込みシステム"

#: ../../english/intro/organization.data:232
msgid "Press Contact"
msgstr "広報窓口"

#: ../../english/intro/organization.data:234
msgid "Web Pages"
msgstr "ウェブページ"

#: ../../english/intro/organization.data:244
msgid "Planet Debian"
msgstr "Planet Debian"

#: ../../english/intro/organization.data:249
msgid "Outreach"
msgstr "教育"

#: ../../english/intro/organization.data:253
msgid "Debian Women Project"
msgstr "Debian Women プロジェクト"

#: ../../english/intro/organization.data:261
msgid "Anti-harassment"
msgstr "嫌がらせ反対"

#: ../../english/intro/organization.data:267
msgid "Events"
msgstr "イベント"

#: ../../english/intro/organization.data:273
#, fuzzy
#| msgid "Technical Committee"
msgid "DebConf Committee"
msgstr "技術委員会"

#: ../../english/intro/organization.data:280
msgid "Partner Program"
msgstr "パートナープログラム"

#: ../../english/intro/organization.data:285
msgid "Hardware Donations Coordination"
msgstr "ハードウェア寄付コーディネーション"

#: ../../english/intro/organization.data:298
msgid "User support"
msgstr "ユーザサポート"

#: ../../english/intro/organization.data:365
msgid "Bug Tracking System"
msgstr "バグ追跡システム (BTS)"

#: ../../english/intro/organization.data:370
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "メーリングリスト管理とメーリングリストアーカイブ"

#: ../../english/intro/organization.data:378
msgid "New Members Front Desk"
msgstr "新規メンバー受付"

#: ../../english/intro/organization.data:384
msgid "Debian Account Managers"
msgstr "Debian アカウント管理者"

#: ../../english/intro/organization.data:389
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""
"(全) DAM に私信のメールを送る際には 57731224A9762EA155AB2A530CA8D15BB24D96F2 "
"の GPG 鍵を使ってください。"

#: ../../english/intro/organization.data:390
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "公開鍵管理者 (PGP および GPG)"

#: ../../english/intro/organization.data:393
msgid "Security Team"
msgstr "セキュリティチーム"

#: ../../english/intro/organization.data:406
msgid "Consultants Page"
msgstr "コンサルタントページ"

#: ../../english/intro/organization.data:411
msgid "CD Vendors Page"
msgstr "CD ベンダページ"

#: ../../english/intro/organization.data:414
msgid "Policy"
msgstr "ポリシー"

#: ../../english/intro/organization.data:419
msgid "System Administration"
msgstr "システム管理"

#: ../../english/intro/organization.data:420
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"こちらの電子メールアドレスは、パスワードに関する問題や、新たなパッケージイン"
"ストールの依頼など、Debianマシンのいずれかに何か問題のある際に用いる連絡先で"
"す。"

#: ../../english/intro/organization.data:430
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Debian マシンに問題があれば、<a href=\"https://db.debian.org/machines.cgi"
"\">Debian マシン</a>のページを参照してください。そこにはマシンごとの管理情報"
"があります。"

#: ../../english/intro/organization.data:431
msgid "LDAP Developer Directory Administrator"
msgstr "LDAP 開発者ディレクトリ管理者"

#: ../../english/intro/organization.data:432
msgid "Mirrors"
msgstr "ミラー"

#: ../../english/intro/organization.data:437
msgid "DNS Maintainer"
msgstr "DNS 管理"

#: ../../english/intro/organization.data:438
msgid "Package Tracking System"
msgstr "パッケージ追跡システム"

#: ../../english/intro/organization.data:440
msgid "Auditor"
msgstr "監事"

#: ../../english/intro/organization.data:446
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr "<a name=\"trademark\" href=\"m4_HOME/trademark\">商標</a>利用申請"

#: ../../english/intro/organization.data:449
msgid "Alioth administrators"
msgstr "Alioth 管理者"

#: ../../english/intro/organization.data:462
msgid "Debian for children from 1 to 99"
msgstr "1歳から99歳までのこどものための Debian"

#: ../../english/intro/organization.data:465
msgid "Debian for medical practice and research"
msgstr "臨床医学と医学研究のための Debian"

#: ../../english/intro/organization.data:468
msgid "Debian for education"
msgstr "教育のための Debian"

#: ../../english/intro/organization.data:473
msgid "Debian in legal offices"
msgstr "法律事務所のための Debian"

#: ../../english/intro/organization.data:477
msgid "Debian for people with disabilities"
msgstr "障害者のための Debian"

#: ../../english/intro/organization.data:481
msgid "Debian for science and related research"
msgstr "科学研究のための Debian"

#~ msgid "Publicity"
#~ msgstr "広報"

#~ msgid "Debian Maintainer (DM) Keyring Maintainers"
#~ msgstr "Debian メンテナ (DM) 公開鍵管理者"

#~ msgid "DebConf chairs"
#~ msgstr "DebConf 役員"
