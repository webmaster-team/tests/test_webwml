#use wml::debian::translation-check translation="1.3"
# $Id: vote_001_quorum.txt,v 1.3 2007/05/06 12:26:37 kreutzm Exp $
# Translator: Helge Kreutzmann <debian@helgefjell.de> 2007-03-02
 Aktuelle Entwickler-Anzahl = 1036
 Q ( sqrt(#devel) / 2 )     = 16,0934769394311
 K min(5, Q )               = 5
 Quorum  (3 x Q )           = 48,2804308182932
