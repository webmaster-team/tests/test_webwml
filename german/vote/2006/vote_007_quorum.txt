#use wml::debian::translation-check translation="1.1"
# $Id: vote_007_quorum.txt,v 1.1 2006/12/16 20:14:58 kreutzm Exp $
# Translator: Helge Kreutzmann <debian@helgefjell.de> 2006-12-16

 Aktuelle Entwickler-Anzahl = 1000
 Q ( sqrt(#devel) / 2 )     = 15,8113883008419
 K min(5, Q )               = 5
 Quorum  (3 x Q )           = 47,4341649025257
